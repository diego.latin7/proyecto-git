// Funciones para mostrar y ocultar los submenús
function mostrarSubMenu(subMenuId) {
    const subMenu = document.getElementById(subMenuId);
    subMenu.style.display = "block";
  }
  
  function ocultarSubMenu(subMenuId) {
    const subMenu = document.getElementById(subMenuId);
    subMenu.style.display = "none";
  }
  
  // Eventos para mostrar/ocultar submenús al hacer clic en cada botón
  ventasServiciosBtn.addEventListener("click", function() {
    mostrarSubMenu("subMenuVentasServicios");
  });
  
  catalogoProductosBtn.addEventListener("click", function() {
    mostrarSubMenu("subMenuCatalogoProductos");
  });
  
  ingresoUsuarioBtn.addEventListener("click", function() {
    mostrarSubMenu("subMenuIngresoUsuario");
  });