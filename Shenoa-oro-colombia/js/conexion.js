const { MongoClient } = require('mongodb');

 async function main() {
    const uri = "mongodb://localhost:27017"; // Cambia esto si tienes una URI diferente

    const client = new MongoClient(uri);

    try {
        await client.connect();

        const database = client.db("usurios_db");
        const collection = database.collection("usuarios");

        const usuarios = await collection.find().toArray();
        console.log(usuarios);
    } catch (error) {
        console.error(error);
    } finally {
        await client.close();
    }
}

main().catch(console.error);
 
