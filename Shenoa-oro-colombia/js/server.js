app.post('/users', async (req, res) => {
  const { nombre, email, password } = req.body;

  try {
    const nuevoUsuario = new Usuario({ nombre, email, password });
    await nuevoUsuario.save(); // Guarda el nuevo usuario en la base de datos
    res.status(201).json(nuevoUsuario);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Error al insertar el usuario' });
  }
});
